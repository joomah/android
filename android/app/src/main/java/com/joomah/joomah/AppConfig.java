package com.joomah.joomah;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;

import com.dd.plist.*;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by weiyin on 3/13/14.
 */
public class AppConfig {
    private static final String TAG = AppConfig.class.getName();


    // singleton
    private static AppConfig mInstance = null;

    // instance variables
    private NSDictionary mDict = null;
    private JSONObject json = null;

    private ArrayList<String> mInternalHosts = null;
    private Integer sidebarBackgroundColor = null;
    private Integer sidebarForegroundColor = null;
    private String initialHost = null;
    private boolean allowZoom = true;
    private String userAgent = null;
    private boolean interceptHtml = false;
    private boolean loginIsFirstPage = false;
    private boolean usesGeolocation = false;
    private boolean showActionBar = true;
    private ArrayList<Pattern> regexInternalExternal = null;
    private ArrayList<Boolean> regexIsInternal = null;
    private ArrayList<Pattern> navStructureLevelsRegex = null;
    private ArrayList<Integer> navStructureLevels = null;
    private ArrayList<HashMap<String,Object>> navTitles = null;
    private HashMap<String,JSONArray> menus = null;


    private AppConfig(Context context){
        InputStream is = null;
        InputStream jsonIs = null;
        try {
            String phantom = "7V70kMJooeFZLi5HquP/vVu7AJNi5DKxhAlwc8iSx44="; // key
            String sparky = "/f1VYmJ5tmUvdFOSStjixA=="; // initilization vector

            if (phantom.length() > 0) {
                SecretKeySpec key = new SecretKeySpec(Base64.decode(phantom), "AES");
                IvParameterSpec iv = new IvParameterSpec(Base64.decode(sparky));
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, key, iv);

                is = context.getApplicationContext().getAssets().open("appConfig.plist.enc");
                CipherInputStream cis = new CipherInputStream(is, cipher);

                mDict = (NSDictionary) PropertyListParser.parse(cis);
            }
            else {
                is = context.getApplicationContext().getAssets().open("appConfig.plist");
                mDict = (NSDictionary) PropertyListParser.parse(is);
            }

            // read json
            jsonIs = context.getAssets().open("appConfig.json");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            IOUtils.copy(jsonIs, baos);
            IOUtils.close(baos);
            this.json = new JSONObject(baos.toString("UTF-8"));

            // preprocess internalHosts
            NSArray hosts = (NSArray) mDict.objectForKey("internalHosts");
            mInternalHosts = new ArrayList<String>(hosts.count());
            for (int i = 0; i < hosts.count(); i++){
                mInternalHosts.add(((NSString)hosts.objectAtIndex(i)).toString());
            }

            // regexInternalExternal
            if (json.has("regexInternalExternal")) {
                JSONArray array = json.getJSONArray("regexInternalExternal");
                int len = array.length();
                this.regexInternalExternal = new ArrayList<Pattern>(len);
                this.regexIsInternal = new ArrayList<Boolean>(len);

                for (int i = 0; i < len; i++) {
                    if (array.isNull(i)) continue;
                    JSONObject entry = array.getJSONObject(i);
                    if (entry != null && entry.has("regex") && entry.has("internal")) {
                        this.regexInternalExternal.add(i, Pattern.compile(entry.getString("regex")));
                        this.regexIsInternal.add(i, entry.getBoolean("internal"));
                    }
                }

            }
            else {
                this.regexInternalExternal = new ArrayList<Pattern>();
                this.regexIsInternal = new ArrayList<Boolean>();
            }

            // preprocess initialHost
            initialHost = Uri.parse(getString("initialURL")).getHost();
            if (initialHost.startsWith("www.")) {
                initialHost = initialHost.substring("www.".length());
            }

            // preprocess colors
            if (mDict.containsKey("androidSidebarBackgroundColor"))
                sidebarBackgroundColor = Color.parseColor(getString("androidSidebarBackgroundColor"));
            if (mDict.containsKey("androidSidebarForegroundColor"))
                this.sidebarForegroundColor = Color.parseColor(getString("androidSidebarForegroundColor"));

            // preprocess allow zoom
            if (mDict.containsKey("allowZoom"))
                allowZoom = getBoolean("allowZoom");
            else
                allowZoom = true;

            // user agent for everything (webview, httprequest, httpurlconnection)
            WebView wv = new WebView(context);
            this.setUserAgent(wv.getSettings().getUserAgentString() + " "
                    + getString("userAgentAdd"));

            // should intercept html
            this.interceptHtml = (containsKey("customCss") && getString("customCss").length() > 0)
                    || (containsKey("stringViewport") && getString("stringViewport").length() > 0)
                    || containsKey("viewportWidth");

            // login is first page
            this.loginIsFirstPage = containsKey("loginIsFirstPage") && getBoolean("loginIsFirstPage");

            // geolocation. Also need to allow permissions in AndroidManifest.xml
            this.usesGeolocation = containsKey("usesGeolocation") && getBoolean("usesGeolocation");

            this.showActionBar = !containsKey("showActionBar") || getBoolean("showActionBar");


            // navStructure
            if (json.has("navStructure") && json.getJSONObject("navStructure").has("urlLevels")) {
                JSONArray urlLevels = json.getJSONObject("navStructure").getJSONArray("urlLevels");
                this.navStructureLevelsRegex = new ArrayList<Pattern>(urlLevels.length());
                this.navStructureLevels = new ArrayList<Integer>(urlLevels.length());
                for (int i = 0; i < urlLevels.length(); i++) {
                    if (urlLevels.isNull(i)) continue;
                    JSONObject entry = urlLevels.getJSONObject(i);
                    if (entry.has("regex") && entry.has("level")) {
                        try {
                            this.navStructureLevelsRegex.add(Pattern.compile(entry.getString("regex")));
                            this.navStructureLevels.add(entry.getInt("level"));
                        } catch (PatternSyntaxException e) {
                            Log.e(TAG, e.getMessage(), e);
                        }
                    }
                }
            }

            if (json.has("navStructure") && json.getJSONObject("navStructure").has("titles")) {
                JSONArray titles = json.getJSONObject("navStructure").getJSONArray("titles");
                this.navTitles = new ArrayList<HashMap<String,Object>>(titles.length());
                for (int i = 0; i < titles.length(); i++) {
                    if (titles.isNull(i)) continue;
                    JSONObject entry = titles.getJSONObject(i);
                    if (entry != null && entry.has("regex")) {
                        try {
                            HashMap<String, Object> toAdd = new HashMap<String, Object>();
                            Pattern regex = Pattern.compile(entry.getString("regex"));
                            toAdd.put("regex", regex);
                            if (entry.has("title")) {
                                toAdd.put("title", entry.getString("title"));
                            }
                            if (entry.has("urlRegex")) {
                                Pattern urlRegex = Pattern.compile(entry.getString("urlRegex"));
                                toAdd.put("urlRegex", urlRegex);
                            }
                            if (entry.has("urlChompWords")) {
                                toAdd.put("urlChompWords", entry.getInt("urlChompWords"));
                            }
                            this.navTitles.add(toAdd);
                        } catch (PatternSyntaxException e) {
                            Log.e(TAG, e.getMessage(), e);
                        }
                    }
                }
            }

            // menus
            JSONObject menus = json.optJSONObject("menus");
            if (menus != null) {
                this.menus = new HashMap<String, JSONArray>();
                Iterator keys = menus.keys();
                while (keys.hasNext()) {
                    String key = (String)keys.next();
                    if (menus.optJSONObject(key) != null &&
                            menus.optJSONObject(key).optJSONArray("items") != null) {
                        this.menus.put(key, menus.optJSONObject(key).optJSONArray("items"));
                    }
                }
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        finally {
            IOUtils.close(is);
            IOUtils.close(jsonIs);
        }
    }

    public synchronized static AppConfig getInstance(Context context){
        if (mInstance == null){
            mInstance = new AppConfig(context.getApplicationContext());
        }
        return mInstance;
    }

    public String getString(String key){
        // json first
        if (json != null && json.has(key)) {
            if (json.isNull(key)) return null;
            else return json.optString(key, null);
        }

        NSString s = (NSString) mDict.objectForKey(key);
        if (s != null)
            return s.toString();
        else
            return null;
    }

    public double getDouble(String key){
        if (json != null && json.has(key)){
            return json.optDouble(key, Double.NaN);
        }

        NSNumber n = (NSNumber) mDict.objectForKey(key);
        if (n != null)
            return n.doubleValue();
        else
            return Double.NaN;
    }

    public boolean getBoolean(String key){
        if (json.has(key))
            return json.optBoolean(key, false);
        else
            return ((NSNumber) mDict.objectForKey(key)).boolValue();
    }

    public boolean containsKey(String key){
        return mDict.containsKey(key) || json.has(key);
    }

    public ArrayList<String> getInternalHosts(){
        return mInternalHosts;
    }

    public Integer getSidebarBackgroundColor() {
        return sidebarBackgroundColor;
    }

    public void setSidebarBackgroundColor(Integer sidebarBackgroundColor) {
        this.sidebarBackgroundColor = sidebarBackgroundColor;
    }

    public Integer getSidebarForegroundColor() {
        return sidebarForegroundColor;
    }

    public void setSidebarForegroundColor(Integer sidebarForegroundColor) {
        this.sidebarForegroundColor = sidebarForegroundColor;
    }

    public String getInitialHost() {
        return initialHost;
    }

    public void setInitialHost(String initialHost) {
        this.initialHost = initialHost;
    }

    public boolean getAllowZoom() {
        return allowZoom;
    }

    public void setAllowZoom(boolean allowZoom) {
        this.allowZoom = allowZoom;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public boolean getInterceptHtml() {
        return interceptHtml;
    }

    public void setInterceptHtml(boolean interceptHtml) {
        this.interceptHtml = interceptHtml;
    }

    public boolean loginIsFirstPage() {
        return loginIsFirstPage;
    }

    public void setLoginIsFirstPage(boolean loginIsFirstPage) {
        this.loginIsFirstPage = loginIsFirstPage;
    }

    public boolean usesGeolocation() {
        return usesGeolocation;
    }

    public void setUsesGeolocation(boolean usesGeolocation) {
        this.usesGeolocation = usesGeolocation;
    }

    public boolean showActionBar() {
        return showActionBar;
    }

    public void setShowActionBar(boolean showActionBar) {
        this.showActionBar = showActionBar;
    }

    public ArrayList<Pattern> getRegexInternalExternal() {
        return regexInternalExternal;
    }

    public ArrayList<Boolean> getRegexIsInternal() {
        return regexIsInternal;
    }

    public ArrayList<Pattern> getNavStructureLevelsRegex() {
        return navStructureLevelsRegex;
    }

    public ArrayList<Integer> getNavStructureLevels() {
        return navStructureLevels;
    }

    public ArrayList<HashMap<String, Object>> getNavTitles() {
        return navTitles;
    }

    public HashMap<String, JSONArray> getMenus() {
        return menus;
    }
}
