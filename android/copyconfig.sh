#!/bin/bash

DIR=`dirname $0`
if [ "$#" -ne 1 ]; then
  echo "Usage: $0 projectid" >&2
  exit 1
fi

scp macmini:prod/build2/builds/$1/config/appConfig.plist $DIR/app/src/main/assets
scp macmini:prod/build2/builds/$1/config/menu_*.json $DIR/app/src/main/assets
scp macmini:prod/build2/builds/$1/config/*_config.json $DIR/app/src/main/res/raw


